class Pueblo < ActiveRecord::Base
  mount_uploader :song, MusicPuebloUploader
  attr_accessible :ciudad, :descFiesta, :descripcion, :estado, :gastronomia, :latitude, :longitude, :lugaresInt, :mesFiesta, :image ,:song, :song_cache, :video
end
