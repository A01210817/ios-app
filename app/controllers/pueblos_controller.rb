class PueblosController < ApplicationController
  # GET /pueblos
  # GET /pueblos.json
  def index


    @ciudads = Pueblo.all
    if(@ciudads)
      @last_modified = Pueblo.order("updated_at").last
      @last_update =@last_modified.updated_at 
    else
      @last_update=DateTime.now.to_date
    end
   #:ciudad, :descFiesta, :descripcion, :estado, :gastronomia, :latitude, :longitude, :lugaresInt, :mesFiesta, :image ,:song, :song_cache
    @ciudads_ = @ciudads.map do |u|
      { :Ciudad => u.ciudad , :Estado => u.estado, :Latitude => u.latitude, :Longitude => u.longitude, :Imagen => u.image, :Descripcion => u.descripcion ,:DescripcionFiesta =>u.descFiesta, :LugaresInteres =>u.lugaresInt, :MesFiesta => u.mesFiesta, :Gastronomia => u.gastronomia, :Song => u.song.url, :Video => u.video}
    end
    @cool_json ={:Title=>"Pueblos Magicos",:LastUpdate=> @last_update, :Ciudades=>@ciudads_ }.to_json
#json = { :users => @userlist, :contacts => current_user.contacts }.to_json
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @cool_json }
    end








    #@pueblos = Pueblo.all

    #respond_to do |format|
    #  format.html # index.html.erb
    #  format.json { render json: @pueblos }
    #end
  end

  # GET /pueblos/1
  # GET /pueblos/1.json
  def show
    @pueblo = Pueblo.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @pueblo }
    end
  end

  # GET /pueblos/new
  # GET /pueblos/new.json
  def new
    @pueblo = Pueblo.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @pueblo }
    end
  end

  # GET /pueblos/1/edit
  def edit
    @pueblo = Pueblo.find(params[:id])
  end

  # POST /pueblos
  # POST /pueblos.json
  def create
    @pueblo = Pueblo.new(params[:pueblo])

    respond_to do |format|
      if @pueblo.save
        format.html { redirect_to @pueblo, notice: 'Pueblo was successfully created.' }
        format.json { render json: @pueblo, status: :created, location: @pueblo }
      else
        format.html { render action: "new" }
        format.json { render json: @pueblo.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /pueblos/1
  # PUT /pueblos/1.json
  def update
    @pueblo = Pueblo.find(params[:id])

    respond_to do |format|
      if @pueblo.update_attributes(params[:pueblo])
        format.html { redirect_to @pueblo, notice: 'Pueblo was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @pueblo.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /pueblos/1
  # DELETE /pueblos/1.json
  def destroy
    @pueblo = Pueblo.find(params[:id])
    @pueblo.destroy

    respond_to do |format|
      format.html { redirect_to pueblos_url }
      format.json { head :no_content }
    end
  end
end
