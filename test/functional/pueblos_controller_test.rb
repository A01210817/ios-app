require 'test_helper'

class PueblosControllerTest < ActionController::TestCase
  setup do
    @pueblo = pueblos(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:pueblos)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create pueblo" do
    assert_difference('Pueblo.count') do
      post :create, pueblo: { ciudad: @pueblo.ciudad, descFiesta: @pueblo.descFiesta, descripcion: @pueblo.descripcion, estado: @pueblo.estado, gastronomia: @pueblo.gastronomia, latitude: @pueblo.latitude, longitude: @pueblo.longitude, lugaresInt: @pueblo.lugaresInt, mesFiesta: @pueblo.mesFiesta }
    end

    assert_redirected_to pueblo_path(assigns(:pueblo))
  end

  test "should show pueblo" do
    get :show, id: @pueblo
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @pueblo
    assert_response :success
  end

  test "should update pueblo" do
    put :update, id: @pueblo, pueblo: { ciudad: @pueblo.ciudad, descFiesta: @pueblo.descFiesta, descripcion: @pueblo.descripcion, estado: @pueblo.estado, gastronomia: @pueblo.gastronomia, latitude: @pueblo.latitude, longitude: @pueblo.longitude, lugaresInt: @pueblo.lugaresInt, mesFiesta: @pueblo.mesFiesta }
    assert_redirected_to pueblo_path(assigns(:pueblo))
  end

  test "should destroy pueblo" do
    assert_difference('Pueblo.count', -1) do
      delete :destroy, id: @pueblo
    end

    assert_redirected_to pueblos_path
  end
end
