class CreatePueblos < ActiveRecord::Migration
  def change
    create_table :pueblos do |t|
      t.string :ciudad
      t.string :estado
      t.float :latitude
      t.float :longitude
      t.text :descripcion
      t.string :mesFiesta
      t.text :lugaresInt
      t.text :descFiesta
      t.text :gastronomia

      t.timestamps
    end
  end
end
